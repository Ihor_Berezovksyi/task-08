package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import com.epam.rd.java.basic.task8.XMLValidation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

   private ArrayList<Flower> flowerList;
    private Flower flower;

    public ArrayList<Flower> DOMParser() {
        if (!XMLValidation.validateXMLSchema("input.xsd", xmlFileName)) return null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = null;

        try {
            doc = dbf.newDocumentBuilder().parse(new File(xmlFileName));
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
            return null;
        }
        flowerList = new ArrayList<>();

        Node rootNode = doc.getFirstChild();
        System.out.println(rootNode.getNodeName());
        NodeList rootChildren = rootNode.getChildNodes();
        for (int k = 0; k < rootChildren.getLength(); k++) {
            flower = new Flower();
            if (rootChildren.item(k).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NodeList flowers = rootChildren.item(k).getChildNodes();
            for (int i = 0; i < flowers.getLength(); i++) {
                if (flowers.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                System.out.println(flowers.item(i).getNodeName());
                switch (flowers.item(i).getNodeName()) {
                    case "name" : {flower.setName(flowers.item(i).getTextContent());
                            break;
                    }
                    case "soil" : flower.setSoil(flowers.item(i).getTextContent()); break;
                    case "origin" : flower.setOrigin(flowers.item(i).getTextContent()); break;
                    case "visualParameters" : {
                        NodeList visualParameters = flowers.item(i).getChildNodes();
                        for (int j = 0; j < visualParameters.getLength(); j++) {
                            if (visualParameters.item(j).getNodeType() != Node.ELEMENT_NODE) {
                                continue;
                            }
                            switch (visualParameters.item(j).getNodeName()) {
                                case "stemColour" : {flower.getVisualParameters().setStemColour(visualParameters.item(j).getTextContent());
                                            break;}
                                case "leafColour" : { flower.getVisualParameters().setLeafColour(visualParameters.item(j).getTextContent());
                                            break;}
                                case "aveLenFlower" : {
                                    flower.getVisualParameters().setMeasuring(visualParameters.item(j).getAttributes().getNamedItem("measure").getTextContent());
                                    flower.getVisualParameters().setAveLenFlower(Integer.parseInt(visualParameters.item(j).getTextContent()));
                                    break;
                                }

                            }
                        }
                        break;
                    }
                    case "growingTips" : {
                        NodeList growingTips = flowers.item(i).getChildNodes();
                        for (int j = 0; j < growingTips.getLength(); j++) {
                            if (growingTips.item(j).getNodeType() != Node.ELEMENT_NODE) {
                                continue;
                            }
                            switch (growingTips.item(j).getNodeName()) {
                                case "tempreture" : {
                                    flower.getGrowingTips().settMeasuring(growingTips.item(j).getAttributes().getNamedItem("measure").getTextContent());
                                    flower.getGrowingTips().setTemperature(Integer.parseInt(growingTips.item(j).getTextContent()));
                                    break;
                                }
                                case "lighting" :{ flower.getGrowingTips().setLightRequirements(growingTips.item(j).getAttributes().getNamedItem("lightRequiring").getTextContent());
                                                break;
                                }
                                case "watering": {
                                    flower.getGrowingTips().setwMeasuring(growingTips.item(j).getAttributes().getNamedItem("measure").getTextContent());
                                    flower.getGrowingTips().setWatering(Integer.parseInt(growingTips.item(j).getTextContent()));
                                    break;
                                }

                            }
                        }
                        break;
                    }
                    case "multiplying" : {flower.setMultiplying(flowers.item(i).getTextContent());
                        break;
                    }

                }

            }
            flowerList.add(flower);
        }
        return flowerList;
    }

    public void DOMWriter(ArrayList<Flower> flowerList) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document doc = null;
        try {
            builder = dbf.newDocumentBuilder();

            if (builder != null) {
                doc = builder.newDocument();
            }
            if (doc != null) {
                Element root = doc.createElement("flowers");
                root.setAttribute("xmlns", "http://www.nure.ua");
                root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
                doc.appendChild(root);
                for (int i = 0; i < flowerList.size(); i++) {
                    Element flower = doc.createElement("flower");
                    root.appendChild(flower);
                    Element name = doc.createElement("name");
                    flower.appendChild(name);
                    name.appendChild(doc.createTextNode(flowerList.get(i).getName()));
                    Element soil = doc.createElement("soil");
                    flower.appendChild(soil);
                    soil.appendChild(doc.createTextNode(flowerList.get(i).getSoil()));
                    Element origin = doc.createElement("origin");
                    flower.appendChild(origin);
                    origin.appendChild(doc.createTextNode(flowerList.get(i).getOrigin()));
                    Element visualParameters = doc.createElement("visualParameters");
                    flower.appendChild(visualParameters);
                    Element stemColour = doc.createElement("stemColour");
                    visualParameters.appendChild(stemColour);
                    stemColour.appendChild(doc.createTextNode(flowerList.get(i).getVisualParameters().getStemColour()));
                    Element leafColour = doc.createElement("leafColour");
                    visualParameters.appendChild(leafColour);
                    leafColour.appendChild(doc.createTextNode(flowerList.get(i).getVisualParameters().getLeafColour()));
                    Element aveLenFlower = doc.createElement("aveLenFlower");
                    visualParameters.appendChild(aveLenFlower);
                    aveLenFlower.setAttribute("measure", flowerList.get(i).getVisualParameters().getMeasuring());
                    aveLenFlower.appendChild(doc.createTextNode(String.valueOf(flowerList.get(i).getVisualParameters().getAveLenFlower())));
                    Element growingTips = doc.createElement("growingTips");
                    flower.appendChild(growingTips);
                    Element temperature = doc.createElement("tempreture");
                    growingTips.appendChild(temperature);
                    temperature.setAttribute("measure", flowerList.get(i).getGrowingTips().gettMeasuring());
                    temperature.appendChild(doc.createTextNode(String.valueOf(flowerList.get(i).getGrowingTips().getTemperature())));
                    Element lighting = doc.createElement("lighting");
                    growingTips.appendChild(lighting);
                    lighting.setAttribute("lightRequiring", flowerList.get(i).getGrowingTips().getLightRequirements());
                    Element watering = doc.createElement("watering");
                    growingTips.appendChild(watering);
                    watering.setAttribute("measure", flowerList.get(i).getGrowingTips().getwMeasuring());
                    watering.appendChild(doc.createTextNode(String.valueOf(flowerList.get(i).getGrowingTips().getWatering())));
                    Element multiplying = doc.createElement("multiplying");
                    flower.appendChild(multiplying);
                    multiplying.appendChild(doc.createTextNode(flowerList.get(i).getMultiplying()));
                }
            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(new FileOutputStream("output.dom.xml")));
        } catch (ParserConfigurationException | FileNotFoundException | TransformerException e) {
            e.printStackTrace();
        }
    }


}